package irvansyah.perben.Mapper;


import irvansyah.perben.Model.TdPembayaran;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TdPembayaranMapper {
    void save(TdPembayaran tdPembayaran);
//    List<TdKeterangan> findAll();
//    TdKeterangan findById(String Id_Keterangan);
//    void update(TdKeterangan tdKeterangan);
//    void deleteData(@Param("Id_Keterangan")String Id_Keterangan);
}
