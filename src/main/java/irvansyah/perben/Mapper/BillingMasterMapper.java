package irvansyah.perben.Mapper;

import irvansyah.perben.Model.BillingHistory;
import irvansyah.perben.Model.BillingMaster;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BillingMasterMapper {
    void save(BillingMaster billingMaster);
    List<BillingMaster> findAll();
    BillingMaster findById(String Id_Billing);
    void update(BillingMaster billingMaster);
    void deleteData(@Param("Id_Billing")String Id_Billing);
}
