package irvansyah.perben.Mapper;



import irvansyah.perben.Model.TdHistory;
import irvansyah.perben.Model.TdKeterangan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TdKeteranganMapper {
    void save(TdKeterangan tdKeterangan);
    List<TdKeterangan> findAll();
    TdKeterangan findById(String Id_Keterangan);
    void update(TdKeterangan tdKeterangan);
    void deleteData(@Param("Id_Keterangan")String Id_Keterangan);
}
