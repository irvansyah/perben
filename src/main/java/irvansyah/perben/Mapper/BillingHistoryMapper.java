package irvansyah.perben.Mapper;

import irvansyah.perben.Model.BillingDetail;
import irvansyah.perben.Model.BillingHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BillingHistoryMapper {
    void save(BillingHistory billingHistory);
    List<BillingHistory> findAll();
    BillingHistory findById(String Id_History);
    void updateData(BillingHistory billingHistory);
    void deleteData(@Param("Id_History")String Id_History);
}
