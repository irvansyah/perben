package irvansyah.perben.Mapper;



import irvansyah.perben.Model.TdHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TdHistoryMapper {
    void save(TdHistory tdHistory);
    List<TdHistory> findAll();
    TdHistory findById(String Id_History);
    void update(TdHistory tdHistory);
    void deleteData(@Param("Id_History")String Id_History);
}
