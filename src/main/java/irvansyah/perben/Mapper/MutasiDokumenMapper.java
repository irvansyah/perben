package irvansyah.perben.Mapper;


import irvansyah.perben.Model.MutasiDokumen;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MutasiDokumenMapper {
    void save(MutasiDokumen mutasiDokumen);
    List<MutasiDokumen> findAll();
    MutasiDokumen findById(String Id_Mutasi);
    void update(MutasiDokumen mutasiDokumen);
    void deleteData(@Param("Id_Mutasi")String Id_Mutasi);
}
