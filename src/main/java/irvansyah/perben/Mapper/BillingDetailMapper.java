package irvansyah.perben.Mapper;

import irvansyah.perben.Model.BillingDetail;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BillingDetailMapper {
    List<BillingDetail> findAll();
    BillingDetail findById (String Id_Billing_detail);
    void saveData(BillingDetail billingDetail);
    void updateData(BillingDetail billingDetail);
    void deleteData(@Param("Id_Billing_Detail")String Id_Billing_Detail);
}
