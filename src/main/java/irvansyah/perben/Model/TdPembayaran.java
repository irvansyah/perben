package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdPembayaran {
    private String Id_Pembayaran;
    private String Kode_Billing;
    private String Id_Header;
    private double Total_Bayar;
    private String Ntpn;
    private String Ntb;
    private String Kode_Bank;
    private double Nomor_Struk_Bayar;


    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("tanggal_dokumen")
    private java.sql.Date Tanggal_Ntpn;


    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("tanggal_expired")
    private java.sql.Date Tanggal_Ntb;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("tanggal_expired")
    private java.sql.Date Tanggal_Buku;
}






















