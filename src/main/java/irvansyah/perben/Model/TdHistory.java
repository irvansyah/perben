package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdHistory {
    private String Id_History;
    private String Id_Header;
    private double Seri;
    private String Kode_Proses;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("waktu_proses")
    private java.sql.Date Waktu_Proses;


    private String Nip_Rekam;


}






















