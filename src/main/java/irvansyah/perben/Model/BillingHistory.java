package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillingHistory {
    private String Id_History;
    private String Id_Billing;
    private String Kode_Status;
    private String Nip_Rekam;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "Asia/Jakarta")
    @JsonProperty("waktu_rekam")
    private Date Waktu_Rekam;



}
