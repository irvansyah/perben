package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TdKeterangan {
    private String Id_Keterangan;
    private String Id_Header;
    private double Seri;
    private String Jenis_Keterangan;
    private String Uraian;
}






















