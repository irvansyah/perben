package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillingMaster {
    private String Id_Billing;
    private String Kode_Billing;
    private String Id_Piutang;
    private String Kode_Status;
    private String Jenis_Dokumen;
    private String Nomor_Dokumen;
    private String Kode_Kantor;
    private double Total_Tagihan;
    private double Total_Bayar;
    private String Currency;
    private String Piutang;
    private String Kd_Id_Wajib_Bayar;
    private String Id_Wajib_Bayar;
    private String Nama_Wajib_Bayar;
    private String Trace_Id;
    private String Trial;
    private double Jumlah_Detail;
    private String Flag_Manual;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("tanggal_dokumen")
    private java.sql.Date Tanggal_Dokumen;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Jakarta")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonProperty("tanggal_expired")
    private java.sql.Date Tanggal_Expired;
}






















