package irvansyah.perben.Model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
//import java.util.Date;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MutasiDokumen {
    private String Id_Mutasi;
    private String Id_Header;
    private String Nomor_Dokumen;
    private String Jenis_Dokumen;
    private String Tanggal_Lunas;
    private String Nip_Petugas_1;
    private String Nip_Petugas_2;


   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "Asia/Jakarta")
   @JsonProperty("tanggal_dokumen")
    private Date Tanggal_Dokumen;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone = "Asia/Jakarta")
    @JsonProperty("tanggal_jatuh_tempo")
    private Date Tanggal_Jatuh_Tempo;
}
