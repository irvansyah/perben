package irvansyah.perben.Model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BillingDetail {
    private String Id_Billing_Detail;
    private String Id_Billing;
    private String Kode_Akun;
    private double Nilai;
    private String Currency;
    private String Id_Wajib_Bayar;
    private String Trace_Id;

}
