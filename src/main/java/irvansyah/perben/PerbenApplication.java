package irvansyah.perben;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerbenApplication {

	public static void main(String[] args) {
		SpringApplication.run(PerbenApplication.class, args);
	}

}
