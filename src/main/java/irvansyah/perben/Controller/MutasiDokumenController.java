package irvansyah.perben.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.MutasiDokumen;
import irvansyah.perben.Service.MutasiDokumenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/V1/MutasiDOkumen")
@Api(description = "api mutasi dokumen")
public class MutasiDokumenController {

    @Autowired
    MutasiDokumenService mutasiDokumenService ;


    @PostMapping(path = "/post",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "/insert")
    public ResponseEntity<?> input(@RequestBody List<MutasiDokumen> mutasiDokumen){

        return mutasiDokumenService.save(mutasiDokumen);
    }
    @GetMapping(path ="/get",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get")
    public ResponseEntity ambilsemua(){

        return mutasiDokumenService.getAll();
    }
    @GetMapping(path = "/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get by id")
    public ResponseEntity getById(@RequestParam String Id_Mutasi){

        return mutasiDokumenService.getId(Id_Mutasi);
    }
    @PutMapping(path = "/put/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "update data")
    public ResponseEntity update(@RequestBody MutasiDokumen mutasiDokumen,@RequestParam String Id_Mutasi){

        return mutasiDokumenService.updateData(mutasiDokumen,Id_Mutasi);
    }
    @DeleteMapping(path = "/delete/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "hapus data by id")
    public ResponseEntity delete (@RequestParam String Id_Mutasi){
        return mutasiDokumenService.deleteById(Id_Mutasi);
    }
}
