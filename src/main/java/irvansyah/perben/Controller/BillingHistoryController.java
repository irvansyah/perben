package irvansyah.perben.Controller;


import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.BillingHistory;
import irvansyah.perben.Service.BillingHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/V1/BillingHistory")
public class BillingHistoryController {
    @Autowired
    BillingHistoryService billingHistoryService;

    @PostMapping(path = "/post",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "insert")
    public ResponseEntity <?> input(@RequestBody List<BillingHistory>billingHistories){

        return billingHistoryService.input(billingHistories);
    }
    @GetMapping(path ="/get",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get")
    public ResponseEntity ambilsemua(){

        return billingHistoryService.getAll();
    }
    @GetMapping(path = "/get{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get by id")
    public ResponseEntity geyById(@RequestParam String Id_History){
        return billingHistoryService.getId(Id_History);
    }
    @PutMapping(path = "/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "update data")
    public ResponseEntity update(@RequestBody BillingHistory billingHistory,@RequestParam String Id_history){

        return billingHistoryService.updateData(billingHistory,Id_history);
    }
    @DeleteMapping(path = "/delete{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "hapus data by id")
    public ResponseEntity delete (@RequestParam String Id_History){
        return billingHistoryService.deleteById(Id_History);
    }
}
