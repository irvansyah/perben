package irvansyah.perben.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.TdHistory;
import irvansyah.perben.Service.TdHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/V1/TdHistory")
@Api(description = "api td history")
public class TdHistoryController {

    @Autowired
    TdHistoryService tdHistoryService;


    @PostMapping(path = "/post",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "/insert")
    public ResponseEntity<?> input(@RequestBody List<TdHistory> tdHistories){

        return tdHistoryService.save(tdHistories);
    }
    @GetMapping(path ="/get",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get")
    public ResponseEntity ambilsemua(){

        return tdHistoryService.getAll();
    }
    @GetMapping(path = "/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get by id")
    public ResponseEntity getById(@RequestParam String Id_History){

        return tdHistoryService.getId(Id_History);
    }
    @PutMapping(path = "/put/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "update data")
    public ResponseEntity update(@RequestBody TdHistory tdHistory,@RequestParam String Id_History){

        return tdHistoryService.updateData(tdHistory,Id_History);
    }
    @DeleteMapping(path = "/delete/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "hapus data by id")
    public ResponseEntity delete (@RequestParam String Id_History){
        return tdHistoryService.deleteById(Id_History);
    }
}
