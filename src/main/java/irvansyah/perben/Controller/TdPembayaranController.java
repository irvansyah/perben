package irvansyah.perben.Controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.TdPembayaran;
import irvansyah.perben.Service.TdPembayaranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/V1/TdPembayaran")
@Api(description = "api td pembayaran")
public class TdPembayaranController {

    @Autowired
    TdPembayaranService tdPembayaranService;


    @PostMapping(path = "/post",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "insert")
    public ResponseEntity<?> input(@RequestBody List<TdPembayaran> tdPembayarans){

        return tdPembayaranService.save(tdPembayarans);
    }
//    @GetMapping(path ="/get",produces = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "get")
//    public ResponseEntity ambilsemua(){
//
//        return tdKeteranganService.getAll();
//    }
//    @GetMapping(path = "/get/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "get by id")
//    public ResponseEntity getById(@RequestParam String Id_Keterangan){
//
//        return tdKeteranganService.getId(Id_Keterangan);
//    }
//    @PutMapping(path = "/put/update",produces = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "update data")
//    public ResponseEntity update(@RequestBody TdKeterangan tdKeterangan,@RequestParam String Id_Keterangan){
//
//        return tdKeteranganService.updateData(tdKeterangan,Id_Keterangan);
//    }
//    @DeleteMapping(path = "/delete/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
//    @ApiOperation(value = "hapus data by id")
//    public ResponseEntity delete (@RequestParam String Id_Keterangan){
//        return tdKeteranganService.deleteById(Id_Keterangan);
//    }
}
