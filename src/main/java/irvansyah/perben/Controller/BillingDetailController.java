package irvansyah.perben.Controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.BillingDetail;
import irvansyah.perben.Service.BillingDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/V1/BillingDetail")
@Api(description = "API untuk Billing Header")

public class BillingDetailController {

    @Autowired
    BillingDetailService billingDetailService;

    @GetMapping(path = "/ambil data",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "mengambil semua data")
    public ResponseEntity getData(){
        return billingDetailService.getData();
    }

    @GetMapping(path = "/get/{id_billing}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get by id")
    public ResponseEntity getById(@RequestParam String Id_Billing_Detail){

        return billingDetailService.getDataId(Id_Billing_Detail);
    }
    @PostMapping(path = "/insertData",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "memasukkan data")
    public ResponseEntity save(@RequestBody List<BillingDetail> billingDetails){

        return billingDetailService.save(billingDetails);
    }
    @PutMapping(path = "/edit data/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "update data")
    public ResponseEntity <?> edit(@RequestBody BillingDetail billingDetail,@RequestParam String Id_Billing_Detail){
        return billingDetailService.updateData(billingDetail,Id_Billing_Detail);
    }
    @DeleteMapping(path = "/hapus/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "hapus data by id")
    public ResponseEntity delete (@PathVariable String Id_Billing_Detail){
        return billingDetailService.deleteById(Id_Billing_Detail);
    }
}
