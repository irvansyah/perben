package irvansyah.perben.Controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import irvansyah.perben.Model.BillingHistory;
import irvansyah.perben.Model.BillingMaster;
import irvansyah.perben.Service.BillingHistoryService;
import irvansyah.perben.Service.BillingMasterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/V1/BillingMaster")
@Api(description = "api billing master")
public class BillingMasterController {
    @Autowired
    BillingMasterService billingMasterService;

    @PostMapping(path = "/billing master",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "/insert")
    public ResponseEntity <?> input(@RequestBody List<BillingMaster>billingMasters){

        return billingMasterService.save(billingMasters);
    }
    @GetMapping(path ="/billing master",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get")
    public ResponseEntity ambilsemua(){

        return billingMasterService.getAll();
    }
    @GetMapping(path = "/billing master/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "get by id")
    public ResponseEntity getById(@RequestParam String Id_Billing){

        return billingMasterService.getId(Id_Billing);
    }
    @PutMapping(path = "/biiling master/update",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "update data")
    public ResponseEntity update(@RequestBody BillingMaster billingMaster,@RequestParam String Id_Billing){

        return billingMasterService.updateData(billingMaster,Id_Billing);
    }
    @DeleteMapping(path = "/billing master/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "hapus data by id")
    public ResponseEntity delete (@RequestParam String Id_Billing){
        return billingMasterService.deleteById(Id_Billing);
    }
}
