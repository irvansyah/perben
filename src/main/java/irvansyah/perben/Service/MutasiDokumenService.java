package irvansyah.perben.Service;

import irvansyah.perben.Mapper.MutasiDokumenMapper;
import irvansyah.perben.Model.MutasiDokumen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


@Service
public class MutasiDokumenService {

    @Autowired
    MutasiDokumenMapper mutasiDokumenMapper;

    public ResponseEntity save(List<MutasiDokumen> mutasiDokumen) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            for (MutasiDokumen dokumensaved :mutasiDokumen) {
                dokumensaved.setId_Mutasi(dokumensaved.getId_Mutasi());
//                System.out.println(billingMaster);
               mutasiDokumenMapper.save(dokumensaved);
            }
                res.put("status", "ok");
                res.put("message", "data berhasil ditambahkan");
                res.put("data",mutasiDokumen);
                return org.springframework.http.ResponseEntity.ok().body(res);
        } catch (Exception e) {
                res.put("status", false);
                res.put("message", e.getMessage());
                return org.springframework.http.ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getAll(){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            List <MutasiDokumen> mutasiDokumen = mutasiDokumenMapper.findAll();
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",mutasiDokumen);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getId(String Id_Mutasi){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
           MutasiDokumen dokumensaved = mutasiDokumenMapper.findById(Id_Mutasi);
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",dokumensaved);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity  updateData(MutasiDokumen mutasiDokumen, String Id_Mutasi){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        MutasiDokumen master  = mutasiDokumenMapper.findById(Id_Mutasi);
        try {
            mutasiDokumen.setId_Mutasi(Id_Mutasi);
            mutasiDokumenMapper.update(mutasiDokumen);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",mutasiDokumen);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_Mutasi){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            mutasiDokumenMapper.deleteData(Id_Mutasi);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_Mutasi);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
