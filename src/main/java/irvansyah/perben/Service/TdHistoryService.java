package irvansyah.perben.Service;

import irvansyah.perben.Mapper.TdHistoryMapper;
import irvansyah.perben.Model.BillingHistory;
import irvansyah.perben.Model.MutasiDokumen;
import irvansyah.perben.Model.TdHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


@Service
public class TdHistoryService {

    @Autowired
    TdHistoryMapper tdHistoryMapper;

    public ResponseEntity save(List<TdHistory> tdHistories) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            for (TdHistory tdHistory :tdHistories) {
                tdHistory.setId_History(tdHistory.getId_History());
//                System.out.println(billingMaster);
                tdHistoryMapper.save(tdHistory);
            }
            res.put("status", "ok");
            res.put("message", "data berhasil ditambahkan");
            res.put("data",tdHistories);
            return org.springframework.http.ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getAll(){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            List <TdHistory> tdHistories= tdHistoryMapper.findAll();
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",tdHistories);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getId(String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
           TdHistory tdHistory = tdHistoryMapper.findById(Id_History);
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",tdHistory);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity updateData(TdHistory tdHistory, String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        TdHistory tdHistory1 = tdHistoryMapper.findById(Id_History);
        try {
            tdHistory.setId_History(Id_History);
            tdHistoryMapper.update(tdHistory);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",tdHistory);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            tdHistoryMapper.deleteData(Id_History);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_History);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
