package irvansyah.perben.Service;



import irvansyah.perben.Mapper.TdPembayaranMapper;
import irvansyah.perben.Model.TdKeterangan;
import irvansyah.perben.Model.TdPembayaran;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


@Service
public class TdPembayaranService {

    @Autowired
    TdPembayaranMapper tdPembayaranMapper;

    public ResponseEntity save(List<TdPembayaran> tdPembayarans) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
       try {
           for (TdPembayaran tdPembayaran:tdPembayarans) {
             tdPembayaran.setId_Pembayaran(tdPembayaran.getId_Pembayaran());
             tdPembayaranMapper.save(tdPembayaran);
           }
           res.put("status","ok");
           res.put("message","berhasil ditambahkan");
           res.put("data",tdPembayarans);
           return ResponseEntity.ok().body(res);
       }catch (Exception e){
           res.put("status",false);
           res.put("message",e.getMessage());
           return ResponseEntity.badRequest().body(res);
       }
    }
//    public ResponseEntity getAll(){
//        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
//        try {
//            List <TdKeterangan> tdKeterangan= tdKeteranganMapper.findAll();
//            res.put("status","ok");
//            res.put("message","data di temukan");
//            res.put("data",tdKeterangan);
//            return ResponseEntity.ok().body(res);
//        }catch (Exception e){
//            res.put("status",false);
//            res.put("message",e.getMessage());
//            return ResponseEntity.badRequest().body(res);
//        }
//    }
//    public ResponseEntity getId(String Id_Keterangan){
//        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
//        try {
//           TdKeterangan tdKeterangan = tdKeteranganMapper.findById(Id_Keterangan);
//            res.put("status","ok");
//            res.put("message","data di temukan");
//            res.put("data",tdKeterangan);
//            return ResponseEntity.ok().body(res);
//        }catch (Exception e){
//            res.put("status",false);
//            res.put("message",e.getMessage());
//            return ResponseEntity.badRequest().body(res);
//        }
//    }
//    public ResponseEntity updateData(TdKeterangan tdKeterangan, String Id_Keterangan){
//        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
//        TdKeterangan tdKeterangan1 = tdKeteranganMapper.findById(Id_Keterangan);
//        try {
//            tdKeterangan.setId_Keterangan(Id_Keterangan);
//            tdKeteranganMapper.update(tdKeterangan);
//            res.put("status","oke");
//            res.put("message","berhasil diupdate");
//            res.put("data",tdKeterangan);
//            return ResponseEntity.ok().body(res);
//        }catch (Exception e){
//            res.put("status",false);
//            res.put("message",e.getMessage());
//            return ResponseEntity.badRequest().body(res);
//        }
//    }
//    public ResponseEntity deleteById(String Id_Keterangan){
//        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
//        try {
//            tdKeteranganMapper.deleteData(Id_Keterangan);
//            res.put("status","ok");
//            res.put("message","data berhasil dihapus");
//            res.put("data",Id_Keterangan);
//            return ResponseEntity.ok().body(res);
//        }catch (Exception e){
//            res.put("status",false);
//            res.put("message",e.getMessage());
//            return ResponseEntity.badRequest().body(res);
//        }
//    }
}
