package irvansyah.perben.Service;

import irvansyah.perben.Mapper.BillingDetailMapper;
import irvansyah.perben.Model.BillingDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.PortUnreachableException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

@Service
public class BillingDetailService {
    @Autowired
    BillingDetailMapper billingDetailMapper;


    public ResponseEntity getData() {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            List<BillingDetail> billingDetails = billingDetailMapper.findAll();
            res.put("status", "ok");
            res.put("message", "ketemu");
            res.put("data", billingDetails);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }

    public ResponseEntity getDataId(String Id_Billing_Detail) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            BillingDetail billingDetails = billingDetailMapper.findById(Id_Billing_Detail);
            res.put("status", "ok");
            res.put("message", "ketemu");
            res.put("data", billingDetails);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity save(List<BillingDetail> billingDetails) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            for (BillingDetail billingDetail :billingDetails) {
                billingDetail.setId_Billing_Detail(billingDetail.getId_Billing_Detail());
                billingDetailMapper.saveData(billingDetail);
            }
            res.put("status", "ok");
            res.put("message", "data berhasil ditambahkan");
            res.put("data",billingDetails);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity <?> updateData(BillingDetail billingDetail,String Id_Billing_Detail){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        BillingDetail billingDetail1 = billingDetailMapper.findById(Id_Billing_Detail);
        try {
            billingDetail.setId_Billing_Detail(Id_Billing_Detail);
            billingDetailMapper.updateData(billingDetail);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",billingDetail);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_Billing_Detail){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            billingDetailMapper.deleteData(Id_Billing_Detail);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_Billing_Detail);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
