package irvansyah.perben.Service;


import irvansyah.perben.Mapper.TdKeteranganMapper;
import irvansyah.perben.Model.TdKeterangan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;


@Service
public class TdKeteranganService {

    @Autowired
    TdKeteranganMapper tdKeteranganMapper;

    public ResponseEntity save(List<TdKeterangan> tdKeterangans) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            for (TdKeterangan tdKeterangan :tdKeterangans) {
                tdKeterangan.setId_Keterangan(tdKeterangan.getId_Keterangan());
//                System.out.println(billingMaster);
                tdKeteranganMapper.save(tdKeterangan);
            }
            res.put("status", "ok");
            res.put("message", "data berhasil ditambahkan");
            res.put("data",tdKeterangans);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getAll(){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            List <TdKeterangan> tdKeterangan= tdKeteranganMapper.findAll();
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",tdKeterangan);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getId(String Id_Keterangan){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
           TdKeterangan tdKeterangan = tdKeteranganMapper.findById(Id_Keterangan);
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",tdKeterangan);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity updateData(TdKeterangan tdKeterangan, String Id_Keterangan){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        TdKeterangan tdKeterangan1 = tdKeteranganMapper.findById(Id_Keterangan);
        try {
            tdKeterangan.setId_Keterangan(Id_Keterangan);
            tdKeteranganMapper.update(tdKeterangan);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",tdKeterangan);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_Keterangan){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            tdKeteranganMapper.deleteData(Id_Keterangan);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_Keterangan);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
