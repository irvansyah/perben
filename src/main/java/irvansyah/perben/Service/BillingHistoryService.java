package irvansyah.perben.Service;

import irvansyah.perben.Mapper.BillingHistoryMapper;
import irvansyah.perben.Model.BillingDetail;
import irvansyah.perben.Model.BillingHistory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public class BillingHistoryService {
    @Autowired
    BillingHistoryMapper billingHistoryMapper;

    public ResponseEntity <?> input(List<BillingHistory>billingHistories){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            for (BillingHistory history: billingHistories){
                history.setId_History(history.getId_History());
                billingHistoryMapper.save(history);
            }
            res.put("status","ok");
            res.put("message","data ditambahkan");
            res.put("data",billingHistories);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getAll(){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            List<BillingHistory> billingHistories = billingHistoryMapper.findAll();
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",billingHistories);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getId(String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
           BillingHistory billingHistories = billingHistoryMapper.findById(Id_History);
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",billingHistories);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity <?> updateData(BillingHistory billingHistory,String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        BillingHistory history = billingHistoryMapper.findById(Id_History);
        try {
            billingHistory.setId_History(Id_History);
            billingHistoryMapper.updateData(billingHistory);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",billingHistory);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_History){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            billingHistoryMapper.deleteData(Id_History);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_History);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
