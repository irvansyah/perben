package irvansyah.perben.Service;

import irvansyah.perben.Mapper.BillingDetailMapper;
import irvansyah.perben.Mapper.BillingMasterMapper;
import irvansyah.perben.Model.BillingDetail;
import irvansyah.perben.Model.BillingHistory;
import irvansyah.perben.Model.BillingMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service
public class BillingMasterService {
    @Autowired
    BillingMasterMapper billingMasterMapper;


    public ResponseEntity save(List<BillingMaster> billingMasters) {
        LinkedHashMap<String, Object> res = new LinkedHashMap<>();
        try {
            for (BillingMaster billingMaster :billingMasters) {
                billingMaster.setId_Billing(billingMaster.getId_Billing());
                System.out.println(billingMaster);
                billingMasterMapper.save(billingMaster);
            }
            res.put("status", "ok");
            res.put("message", "data berhasil ditambahkan");
            res.put("data",billingMasters);
            return ResponseEntity.ok().body(res);
        } catch (Exception e) {
            res.put("status", false);
            res.put("message", e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getAll(){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            List<BillingMaster> billingMasters = billingMasterMapper.findAll();
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",billingMasters);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity getId(String Id_Billing){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            BillingMaster billingMaster = billingMasterMapper.findById(Id_Billing);
            res.put("status","ok");
            res.put("message","data di temukan");
            res.put("data",billingMaster);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity  updateData(BillingMaster billingMaster, String Id_Billing){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        BillingMaster master  = billingMasterMapper.findById(Id_Billing);
        try {
            billingMaster.setId_Billing(Id_Billing);
            billingMasterMapper.update(billingMaster);
            res.put("status","oke");
            res.put("message","berhasil diupdate");
            res.put("data",billingMaster);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
    public ResponseEntity deleteById(String Id_Billing){
        LinkedHashMap<String,Object> res = new LinkedHashMap<>();
        try {
            billingMasterMapper.deleteData(Id_Billing);
            res.put("status","ok");
            res.put("message","data berhasil dihapus");
            res.put("data",Id_Billing);
            return ResponseEntity.ok().body(res);
        }catch (Exception e){
            res.put("status",false);
            res.put("message",e.getMessage());
            return ResponseEntity.badRequest().body(res);
        }
    }
}
